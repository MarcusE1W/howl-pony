-- Copyright 2012-2018 The Howl Developers
-- License: MIT (see LICENSE.md at the top-level directory of the distribution)

mode_reg =
  name: 'pony'
  extensions: { 'pony' }
  create: -> bundle_load('pony_mode')

howl.mode.register mode_reg
howl.inspection.register {
  name: 'pony',
  factory: (buffer) ->
    bundle_load('pony_inspector') buffer
}

unload = ->
  howl.mode.unregister 'pony'
  howl.inspection.unregister 'pony'

return {
  info:
    author: 'Copyright 2018 The Howl Developers',
    description: 'Pony lexer bundle',
    license: 'MIT',
  :unload
}
