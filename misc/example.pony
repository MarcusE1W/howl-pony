"""
An example to test Pony syntax.
This is a "long" string.
\"""
use "collections"

// single comment

/*
Block comment /* with nested comment // */
*/

primitive \annotation1, annotation2\ NotU32
type MaybeU32 is (U32 | NotU32)

interface Animal
  fun talk(): String

trait HasTail

class Dog is (Animal & HasTail)
  let _out: OutStream
  var _vocal: Bool = false
  new create(out': OutStream) => _out = out'
  fun ref set_vocal(vocal': Bool): Bool =>
    _vocal = vocal'
  fun talk(): String => if _vocal then "Woof, Woof!" else "Woof" end

actor Walker
  let _out: OutStream
  new create(out': OutStream) => _out = out'
  be walk() => _out.print("walking...")

actor Main
  new create(env: Env) =>
    env.out.print("A \"string\"")

    let dog = Dog(env.out) .> set_vocal(true)

    let tuple = (I8(10), "green", "bottles")
    (let number, let _, let string) = tuple
    let t1'''': String = tuple._2
    let hex: U32 = 0xd3adbeef
    let n2: I32 = -1_000_000
    let n3: I32 = -0b10101
    let n4: I32 = -0B_1_1
    let n5: I32 = -0X_a_b
    let n6 = U32('😃12345')
    let float: F32 = -3_3.4_4e-_1_1
    let incrementer: {(I64): I64} = {(n) => n + 1}

    ifdef windows or linux then
      env.out.print("on windows or linux")
    end

    let map = Map[String, Any]
      .> update("a", Map[String, String] .> update("a1", "v1"))
      .> update("b", "v2")
    map("c") = "v3"

