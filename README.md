![Pony](pony.jpg)

Lexer bundle for the Howl editor (http://howl.io/)

(Version 0.4.1)

# Contains
- **Pony:** Bundle for a language supporting parallell programming with the actor model in a safe way

# These functions are supported:
1. Inspector to show error messages on save
1. Syntax highlighting
2. Support for the `buffer-structure` command ( Alt+s )
3. Auto indentation
4. Some build in code blocks (snippets)

# Installation

Clone or download into your local Howl folder `~/.howl/bundles`

e.g.:

```bash
git clone https://gitlab.com/MarcusE1W/howl-pony ~/.howl/bundles/
```
