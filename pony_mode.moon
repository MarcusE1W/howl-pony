-- Copyright 2012-2018 The Howl Developers
-- License: MIT (see LICENSE.md at the top-level directory of the distribution)

append = table.insert

{
  lexer: bundle_load('pony_lexer')

  comment_syntax: '//'
  word_pattern: r'\\b[\\pL_][\\pL\\pN_]+\\b'

  default_config:
    inspectors_on_save: { 'pony' }

  indentation: {
    more_after: {
      r'^\\s*(else|elseif)\\b'
      r'=>'
      r'\\b(try|object|recover|else)\\s*$'
      r'\\b(then|do)\\s*$'
      '[[{(]%s*$'
    }

    less_for: {
      r'^\\s*(actor|class|primitive|trait|interface|end|else|elseif|then)\\b'
      r'^\\s*[]}\\)]'
    }
  }

  auto_pairs: {
    '(': ')'
    '[': ']'
    '{': '}'
    '"': '"'
  }

  -- define keywords that create a buffer-structure (Alt-s)
  structure: (editor) =>
    lines = {}

    patterns = {
      '^%s*actor%s+'
      '^%s*class%s+'
      '^%s*primitive%s+'
      '^%s*trait%s+'
      '^%s*interface%s+'
      '^%s*new%s+'
      '^%s*fun%s+'
      '^%s*be%s+'
    }

    for line in *editor.buffer.lines
      for p in *patterns
        if line\umatch p
          append lines, line

    return lines

  -- define code blocks (snippets)
  code_blocks:
    multiline: {
      { r'\\s+(then|do)\\s*$', '^%s*end', 'end' },
      { r'\\b(try|else)\\s*$', r'^\\s*(end|else|then)', 'end' },
      { r'\\b(match|object|recover)\\b', r'^\\s*(end|else)', 'end' },
      { r'\\brepeat\\s*$', '^%s*end', 'until false end' },
      { '{%s*$', '^%s*}', '}'}
      { '%(%s*$', '^%s*%)', ')'}
      { '%[%s*$', '^%s*%]', ']'}
      { '"""%s*$', '^%s*"""', '"""'}
    }
}
